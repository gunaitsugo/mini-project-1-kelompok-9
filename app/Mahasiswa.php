<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = [
        'nama', 'nim', 'fakultas', 'jurusan'
    ];

    public function user(){
        return $this->hasOne(User::class);
    }
}
