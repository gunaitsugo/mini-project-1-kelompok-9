<?php

namespace App\Models\Loan;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Loan extends Model
{
    protected $fillable=[
      'user_id','borowwed_date','returned_deadline','returned_date','isOntime','isCheckout',
    ];

    public function getRouteKeyName(){
        return 'id';
    }
    // relationship: satu loan bisa dimiliki satu user
    public function user(){
      return $this->belongsTo(User::class);
    }
    //relationship: satu loan mempunyai banyak loan detail
    public function loandetail(){
      return $this->hasMany(LoanDetail::class);
    }
}
