<?php

namespace App\Models\Loan;


use Illuminate\Database\Eloquent\Model;
use App\Models\Buku;
class LoanDetail extends Model
{
    //
    protected $fillable=[
      'loan_id','book_id','quantity'
    ];
    public function getRouteKeyName(){
        return 'id';
    }
    // protected $with =['user', 'book'];

    // relationship : satu loan_detail hanya punya satu jenis buku
    public function book(){//perlu ditambahin use kelas book nya
      return $this->belongsTo(Buku::class);
    }

    // relationship : satu loan_detail dimiliki oleh satu loan
    public function loan(){
      return $this->belongsTo(Loan::class);
    }
}
