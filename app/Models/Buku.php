<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
Use App\Models\Loan\LoanDetail;
class Buku extends Model
{
    protected $table = 'buku';
    protected $guarded = [];

    public function getRouteKeyName(){
        return 'kode_buku';
    }

    public function loandetail()
    {
        return $this->hasMany(LoanDetail::class);
    }
}
