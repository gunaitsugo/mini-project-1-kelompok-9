<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryBuku extends Model
{
    protected $table = 'category_buku';
    protected $fillable = ['category_name'];
    public $timestamps = false;
    public function getRouteKeyName(){
        return 'id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
