<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
              'id'=>$this->id,
              'id_pinjaman' => $this->loan->id,
              'tanggal_pinjam' => $this->loan->borowwed_date,
              'batas_peminjaman' => $this->loan->borowwed_date,
              'email_peminjam'=> $this->loan->user->email,
              'kode_buku' => $this->book->kode_buku,
              'buku_yang_dipinjam' => $this->book->judul,
              'jumlah_buku'=>$this->quantity,
          ];
    }
}
