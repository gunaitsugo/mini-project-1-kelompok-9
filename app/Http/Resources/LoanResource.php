<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $buku=[];
      $jumlah=[];
      foreach ($this->loandetail as $key => $value) {
        $buku[]=$value->book->judul;
        $jumlah[]=$value->quantity;
      }
      return [
            'id'=>$this->id,
            'tanggal_pinjam' => $this->borowwed_date,
            'batas_peminjaman' => $this->borowwed_date,
            'email_peminjam'=> $this->user->email,
            'buku_yang_dipinjaman'=>$buku,
            "jumlah" => $jumlah,
        ];
    }
}
