<?php

namespace App\Http\Controllers\Mahasiswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mahasiswa;
use App\Http\Resources\MahasiswaResource;
use PHPUnit\Framework\Constraint\IsEmpty;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Mahasiswa::all();
        return MahasiswaResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nama' => 'required',
            'nim' => ['required', 'unique:mahasiswas,nim'],
            'fakultas' => 'required',
            'jurusan' => 'required'
        ]);

        if(Mahasiswa::where('user_id', auth()->user()->id)->count() == 0){
            $mahasiswa = auth()->user()->mahasiswa()->create($this->storeMahasiswa());
            return $mahasiswa;
        }
        else{
            return response('1 akun hanya memiliki 1 data mahasiswa',200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nim)
    {
        $data = Mahasiswa::where('nim', $nim)->get();
        return new MahasiswaResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nim)
    {
        request()->validate([
            'nama' => 'required',
            'nim' => 'required',
            'fakultas' => 'required',
            'jurusan' => 'required'
        ]);

        auth()->user()->mahasiswa()->update($this->storeMahasiswa());

        $data = Mahasiswa::where('nim', $nim)->get();
        return new MahasiswaResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        Mahasiswa::where('nim', $nim)->delete();

        return response('Data Mahasiswa telah dihapus', 200);
    }

    public function storeMahasiswa(){
        return [
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan')
        ];
    }
}
