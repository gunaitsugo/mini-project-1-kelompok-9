<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\sosialaccount;
use App\User;
use Laravel\Socialite\Facades\Socialite;

class socialtecontroller extends Controller
{

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleprovidercallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

        return redirect('/home');

    }

    public function findOrCreateUser($SocialUser, $provider){
        $sosialaccount = sosialaccount::where('provider_id', $SocialUser->getID())
                            ->where('provider_name', $provider)
                            ->first();

        if($sosialaccount){
            return $sosialaccount->user;
        } else {
            $user = User::where('email', $SocialUser->getEmail())->first();

            if (! $user) {
                $user = User::create([
                    'name ' => $SocialUser->getName(),
                    'email' => $SocialUser->getEmail()
                ]);
            }

            $user->sosialaccount()->create([
                'Provider_id' => $SocialUser->getId(),
                'Provider_name' => $provider

            ]);

            return $user;
        }
    }
    
}
