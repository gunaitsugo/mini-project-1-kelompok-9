<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['required','email','unique:users,email'],
            'no_hp' => ['required','numeric','unique:users,no_hp'],
            'password' => ['required', 'min:8']
        ]);

        User::create([
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'password' => Hash::make($request->password)
        ]);

        return response('Terima kasih, Anda telah terdaftar',200);
    }
}
