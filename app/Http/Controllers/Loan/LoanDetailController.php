<?php

namespace App\Http\Controllers\Loan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan\LoanDetail;
use App\Http\Resources\LoanDetailResource;
use App\Http\Resources\LoanDetailCollection;
use App\Models\Buku;

class LoanDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $loandetails= LoanDetail::get();
      return new LoanDetailCollection($loandetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        $request-> validate([
          'book_id' => ['required'],
          'quantity'=> ['required']
        ]);
        $book_id=request('book_id');
        $quantity=request('quantity');
        $loandetails = LoanDetail::create([
          'loan_id' => $id,
          'book_id' => $book_id,
          'quantity' => $quantity,
        ]);
        $update_stock=Buku::where('id',$book_id)->
                      update([
                      'stock' =>((Buku::find($book_id))->stock)-$quantity
        ]);

        return $loandetails;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(LoanDetail $loandetail)
    {
        return new LoanDetailResource($loandetail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanDetail $loandetail)
    {
      $request-> validate([
        'book_id' => ['required'],
        'quantity'=> ['required']
      ]);

      $loandetails = LoanDetail::update([
        'book_id' => request('book_id'),
        'quantity' => request('quantity'),
      ]);

      return $loandetails;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanDetail $loandetails)
    {
      $loandetails->delete();
      return response()->json('the loandetail have been deleted', 200);
    }
}
