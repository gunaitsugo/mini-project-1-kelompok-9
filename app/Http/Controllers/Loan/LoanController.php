<?php

namespace App\Http\Controllers\Loan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\User;
use App\Models\Loan\Loan;
use App\Http\Controllers\Loan\Auth;
use App\Http\Resources\LoanCollection;
use App\Http\Resources\LoanResource;


class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
      $loans = Loan::get();
      return new LoanCollection($loans);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $now= Carbon::now();
        $loans = Loan::create([
          'user_id' => auth()->user()->id,
          'borowwed_date' => date($now),
          'returned_deadline'=>date($now->addDay(7)),
        ]);
        return new LoanResource($loans);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
      return new LoanResource($loan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     // hanya admin yang bisa mengupdate returned_sdata
    public function update(Request $request, Loan $loan)
    {
      $request-> validate([
        'returned_date' => ['required'],
        ]);

      $loan->update([
          'returned_date' => $request->returned_date,
          'isOntime' => (($loan->returned_deadline)>=(request('returned_date'))),
        ]);

      return $loan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
      $loan->delete();
      return response()->json('the loan has been deleted', 200);
    }

}
