<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->group(function(){
    Route::post('/register', 'RegisterController');
    Route::post('/login', 'LoginController');
    Route::post('/logout', 'LogoutController');
    Route::get('/auth/{provider}', 'socialtecontroller@redirectToProvider');
    Route::get('/auth/{provider}/callback', 'socialtecontroller@handleprovidercallback');
});

//Middleware auth dan admin saja
Route::namespace('Perpustakaan')->middleware('auth:api','admin')->group(function(){
    //Category Buku
    Route::post('create-category-buku', 'CategoryController@store');
    Route::patch('update-category-buku/{category_buku}', 'CategoryController@update');
    Route::delete('delete-category-buku/{category_buku}', 'CategoryController@destroy');
    Route::get('category-buku/{category_buku}', 'CategoryController@show');
    Route::get('category-buku', 'CategoryController@index');

    //Buku
    Route::post('create-buku', 'BukuController@store');
    Route::patch('update-buku/{buku}', 'BukuController@update');
    Route::delete('delete-buku/{buku}', 'BukuController@destroy');
    Route::get('buku/{buku}', 'BukuController@show');
    Route::get('buku', 'BukuController@index');
});

//Middleware auth
Route::namespace('Loan')->middleware('auth:api')->group(function(){
    //loandetail
    Route::post('create-loandetail/{loan_id}', 'LoanDetailController@store');
    Route::patch('update-loandetail/{loandetail}', 'LoanDetailController@update');
    Route::get('loandetail', 'LoanDetailController@index')->middleware('admin');
    Route::get('loandetail/{loandetail}', 'LoanDetailController@show');
    Route::delete('delete-loandetail/{loandetail}', 'LoanDetailController@destroy')->middleware('admin');

    //Loan
    Route::post('create-loan', 'LoanController@store');
    Route::patch('update-loan/{loan}', 'LoanController@update');
    Route::get('loan', 'LoanController@index');
    Route::get('loan/{loan}', 'LoanController@show');
    Route::delete('delete-loan/{loan}', 'LoanController@destroy')->middleware('admin');
});

//Middleware auth
//Route Mahasiswa
Route::namespace('Mahasiswa')->middleware('auth:api')->group(function() {
    Route::get('/mahasiswa', 'MahasiswaController@index');
    Route::post('/mahasiswa/create', 'MahasiswaController@store');
    Route::get('/mahasiswa/{nim}', 'MahasiswaController@show');
    Route::patch('/mahasiswa/{nim}/update', 'MahasiswaController@update');
    Route::delete('/mahasiswa/{nim}/delete', 'MahasiswaController@destroy');
});
