<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Buku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_buku',10)->unique();
            $table->unsignedBigInteger('category_id');
            $table->string('judul');
            $table->string('pengarang');
            $table->integer('tahun_terbit');
            $table->integer('stock')->default(50);
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('category_buku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');

    }
}
