<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'role_name' => 'mahasiswa'
        ]);

        Role::create([
            'role_name' => 'admin'
        ]);
    }
}
